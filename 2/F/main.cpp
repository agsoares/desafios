//10954 - Add All
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int n;
    while (cin >> n) {
        if (n == 0) break;
        int val, cost = 0;
        priority_queue<int, vector<int>, greater<int> > Q;
        for (int i = 0; i < n; i++) {
            cin >> val;
            Q.push(val);
        }
        while (Q.size() > 1) {
            int a = Q.top();
            Q.pop();
            int b = Q.top();
            Q.pop();
            Q.push(a+b);
            cost += (a+b);
        }
        Q.pop();
        cout << cost << endl;
    }
    return 0;
}
