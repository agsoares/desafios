//556 - Amazing
#include <iostream>
#include <iomanip>

using namespace std;
int main(int argc, char** argv) {
    int b, w;
    while (cin >> b >> w) {
        int output[] = {0, 0, 0, 0, 0};
        int state = 0;
        int origin_x, origin_y, curr_x, curr_y;
        if (b == 0 || w == 0) break;
        int maze[b+2][w+2];
        for (int i = 0; i <= b+1; i++) {
            for (int j = 0; j <= w+1; j++) {
                int val = -1;
                if (!(i == 0 || i == b+1 || j == 0 || j == w+1)) {
                    char x;
                    cin >> x;
                    val = x == '1' ? -1 : x-'0';
                }
                maze[i][j] = val;
            }
        }
        origin_x = 1;
        origin_y = b;
        curr_x = origin_x;
        curr_y = origin_y;
        while (1) {
            switch (state) {
                case 0: //RIGHT
                    if (maze[curr_y][curr_x+1] != -1) {
                        maze[curr_y][curr_x]++;
                        curr_x++;
                    } else {
                        state++;
                        break;
                    }
                    if (maze[curr_y+1][curr_x] != -1) {
                        state = 3;
                    }
                break;
                case 1: //UP
                    if (maze[curr_y-1][curr_x] != -1) {
                        maze[curr_y][curr_x]++;
                        curr_y--;
                    } else {
                        state++;
                        break;
                    }
                    if (maze[curr_y][curr_x+1] != -1) {
                        state--;
                    }

                break;
                case 2: //LEFT
                    if (maze[curr_y][curr_x-1] != -1) {
                        maze[curr_y][curr_x]++;
                        curr_x--;
                    } else {
                        state++;
                        break;
                    }
                    if (maze[curr_y-1][curr_x] != -1) {
                        state--;
                    }
                break;
                case 3: //DOWN
                    if (maze[curr_y+1][curr_x] != -1) {
                        maze[curr_y][curr_x]++;
                        curr_y++;
                    } else {
                        state=0;
                        break;
                    }
                    if (maze[curr_y][curr_x-1] != -1) {
                        state--;
                    }
                break;
            }
            if (curr_x == origin_x && curr_y == origin_y) break;
        }
        for (int i = 0; i <= b+1; i++) {
            for (int j = 0; j <= w+1; j++) {
                if (maze[i][j] != -1) {
                    output[maze[i][j]]++;
                }
            }
        }

        cout << setw(3) << output[0];
        cout << setw(3) << output[1];
        cout << setw(3) << output[2];
        cout << setw(3) << output[3];
        cout << setw(3) << output[4];
        cout << endl;

    }
    return 0;
}
