//11559 - Event Planning
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    int n, b, h, w;

    while (cin >> n >> b >> h >> w) {
        int cost = 0;
        int beds = 0;
        int best_price = b+1;
        for (int i = 0; i < h; i++) {
            cin >> cost;
            for (int j = 0; j < w; j++) {
                cin >> beds;
                if (cost*n < best_price && beds >= n) {
                    best_price = cost*n;
                }
            }
        }
        if (best_price > b) {
            cout << "stay home" << endl;
        } else {
            cout << best_price << endl;
        }
    }
    return 0;
}
