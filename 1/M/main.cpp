//11459 - Snakes and Ladders
#include <iostream>

using namespace std;

typedef struct ladder {
    int bottom;
    int top;
} ladder;

int main(int argc, char** argv) {
    int test_cases;
    cin >> test_cases;
    for (int i = 0; i < test_cases; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        int players[a];
        for (int j = 0; j < a; j++) {
            players[j] = 1;
        }
        ladder ladders[b];
        for (int j = 0; j < b; j++) {
            cin >> ladders[j].bottom >> ladders[j].top;
        }
        int won = 0;
        for (int j = 0; j < c; j++) {
            int roll;
            cin >> roll;
            int player_pos = players[j%a] + roll;
            if (won) continue;
            for (int k = 0; k < b; k++) {
                if (ladders[k].bottom == player_pos) {
                    player_pos = ladders[k].top;
                    break;
                }
            }
            if (player_pos >= 100) {
                player_pos = 100;
                won = 1;
            }
            players[j%a] = player_pos;
        }

        for (int j = 0; j < a; j++) {
            cout << "Position of player " << j+1 << " is " << players[j] << "." << endl;
        }

    }
    return 0;
}
