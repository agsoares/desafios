//11060 - Beverages
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef vector<int> vi;
vector <vi> list;

map<string, int> beverages;
map<int, string> names;

int main(int argc, char** argv) {
    int n, e, c = 0;
    string beverage, b1, b2;
    while (cin >> n) {
        int counter[n], b = 0;
        for(int i = 0; i < n; i++) {
            cin >> beverage;
            beverages[beverage] = i;
            names[i] = beverage;
            counter[i] = 0;
        }
        list.assign(n, vi());
        cin >> e;
        for(int i = 0; i < e; i++) {
            cin >> b1 >> b2;
            list[beverages[b1]].push_back(beverages[b2]);
            counter[beverages[b2]]++;
        }
        cout << "Case #" << ++c << ": Dilbert should drink beverages in this order:";
        while (b < n) {
            for(int i = 0; i < n; i++) {
                if (counter[i] == 0) {
                    counter[i]--;
                    int sel = i;
                    cout << " ";
                    cout << names[sel];
                    for (vi::iterator j = list[sel].begin(); j != list[sel].end(); j++) {
                      counter[*j]--;
                    }
                    b++;
                    break;
                }
            }
        }
        cout << "." << endl << endl;

        list.clear();
        names.clear();
        beverages.clear();
    }
    return 0;
}
