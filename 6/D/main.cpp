//10047 - The Monocycle
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>

#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <functional>

#define MAX_COLORS 5;

using namespace std;
int m, n, startX, startY, targetX, targetY;
int color = 0, minT;
bool possible;

char map[25][25]
bool visited[25][25];

int left(int i)  { return i-1 < 0 ? 3 : i-1; }
int right(int i) { return (i+1)%4; }

void dfs(int x, int y, int dir, int t, int color, bool found) {
    visited[x][y] = true;

}


int main(int argc, char** argv) {
    int c = 0;
    while (cin >> m >> n) {
        if (n == 0) break;
        if (c > 0) cout << endl;
        minT = 999999; color = 0;
        possible = false;
        char tile;
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < m; j++) {
                visited[i][j] = false;
                cin >> tile;
                map[i][j] = tile;
                if (tile == 'S') {
                    startX = i; startY = j;
                    visited[i][j] = true;

                }
                if (tile == 'T') {
                    targetX = i; targetY = j;
                }
            }
        }
        dfs(startX, startY, 0, 0, 0, false);

        cout << "Case #" << ++c << endl;
        if (possible) cout << "minimum time = " << minT << " sec" << endl;
        else cout << "“destination not reachable" << endl;
    }
    return 0;
}
