//10004 - Bicoloring
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;

int *color;
int bicolor;
vector<vii> list;

void dfs(int parent, int vertex) {
  if (parent == -1)  color[vertex] = 0;
  else if (color[vertex] == -1)
      color[vertex] = color[parent] == 0 ? 1 : 0;
  for (vii::iterator i = list[vertex].begin(); i != list[vertex].end(); i++) {
    if (color[i->first] == -1) dfs(vertex, i->first);
    else if (color[vertex] == color[i->first]) bicolor = 0;
  }
}

int main(int argc, char** argv) {
    int n, e, v1, v2;
    while (cin >> n) {
        if (n == 0) break;
        color = new int[n];
        bicolor = 1;
        for (int i = 0; i < n; i++ ) {
          color[i] = -1;
        }
        list.assign(n, vii());
        cin >> e;
        for (int i = 0; i < e; i++ ) {
          cin >> v1 >> v2;
          list[v1].push_back(ii(v2, 1));
          list[v2].push_back(ii(v1, 1));
        }
        dfs(-1, 0);
        list.clear();
        if (bicolor) cout << "BICOLORABLE." << endl;
        else  cout << "NOT BICOLORABLE." << endl;
    }
    return 0;
}
