//821 - Page Hopping
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

#define MAX_N 100
#define INF 9999999

typedef pair<int, int> ii;
typedef vector<ii> vii;

vector<vii> list;

int g[MAX_N][MAX_N];

void floyd() {
    for (int k = 0; k < MAX_N; k++) {
        for (int i = 0; i < MAX_N; i++) {
            for (int j = 0; j < MAX_N; j++) {
                if(g[i][j] > g[i][k] + g[k][j])
                    g[i][j] = g[i][k] + g[k][j];
            }
        }
    }
}

int main(int argc, char** argv) {
    int a, b, links = 0;
    int c = 0;
    while (cin >> a && cin >> b) {
        if (links == 0) {
            if (a == 0 && b == 0) break;
            for (int i = 0; i < MAX_N; i++) {
                for (int j = 0; j < MAX_N; j++) {
                    if (i == j) g[i][j] = 0;
                    else g[i][j] = INF;
                }
            }
        }
        if (a != 0 && b != 0) {
            g[a-1][b-1] = 1;
            links++;
        } else {
            links = 0;
            floyd();
            float count = 0, val = 0;
            for (int i = 0; i < MAX_N; i++) {
                for (int j = 0; j < MAX_N; j++) {
                    if(g[i][j] < INF && g[i][j] > 0) {
                        count++;
                        val += g[i][j];
                    }
                }
            }
            cout.precision(3);
            cout.setf(ios::fixed, ios::floatfield);
            cout << "Case " << ++c << ": average length between pages = ";
            if (count > 0)
                cout << val/count;
            else
                cout << val;
            cout << " clicks" << endl;
        }
    }
    return 0;
}
