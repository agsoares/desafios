//11733 - Airports
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>

#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;
vector <vii> list;

bool *visited;

int prim(int vi, int n, int a) {
    priority_queue< pair<int,int>, vector< pair<int,int> >, greater< pair<int,int> > > q;
    pair<int,int> p;
    int v,w, ret = 0;
    q.push( make_pair(0, vi));
    while (!q.empty()) {
        p = q.top(); q.pop();
        v = p.second;
        w = p.first;
        if (visited[v] == true) continue;
        visited[v] = true;
        ret += w;
        for (int i = 0; i < list[v].size(); ++i)
            if (!visited[list[v][i].first]) {
                q.push( make_pair( list[v][i].second, list[v][i].first ) );
            }
    }
    return ret;
}

int all_visited(int n) {
    for (int i = 0; i < n; i++) {
        if (visited[i] == false) return i;
    }
    return -1;
}

int main(int argc, char** argv) {
    int tests = 0;
    cin >> tests;
    int cost = 0, airports = 0;
    for (int t = 0; t < tests;) {
        int n, m, a;
        airports = 0; cost = 0;
        cin >> n >> m >> a;
        list.assign(n, vii());
        int v1, v2, w;
        for (int i = 0; i < m; i++) {
            cin >> v1 >> v2 >> w;
            if (w >= a) continue;
            list[v1-1].push_back(ii(v2-1, w));
            list[v2-1].push_back(ii(v1-1, w));
        }
        visited = new bool[n];
        for (int i = 0; i < n; i++) {
            visited[i] = false;
        }
        int v;
        while ((v = all_visited(n)) >= 0) {
            cost += prim(v, n, a);
            cost += a;
            airports++;
        }

        cout << "Case #" << ++t << ": ";
        cout << cost << " " << airports << endl;
    }

    return 0;
}
