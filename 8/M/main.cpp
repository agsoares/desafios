//10986 - Sending email
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;
vector <vii> list;

int dijkstra(int a, int b, int n) {
    priority_queue< pair<int,int>, vector< pair<int,int> >, greater< pair<int,int> > > q;
    vector< int > d(n, -1);
    pair<int,int> p; int v,w;
    q.push( make_pair(0,a) );
    while (!q.empty()) {
        p = q.top(); q.pop();
        v = p.second;
        w = p.first;
        if ( d[v] != -1 ) continue;
        if (v == b) return w;
        d[v] = w;
        for (int i = 0; i < list[v].size(); ++i)
            if ( d[ list[v][i].first ] == -1 ) {
                q.push( make_pair( w+list[v][i].second, list[v][i].first ) );
            }
    }
    return -1;
}

int main(int argc, char** argv) {
    int tests = 0;
    cin >> tests;
    for (int c = 0; c < tests;) {
        int n, m, s, t;
        int ms = 0;
        cin >> n >> m >> s >> t;
        list.assign(n, vii());
        int v1, v2, w;
        for (int i = 0; i < m; i++) {
            cin >> v1 >> v2 >> w;
            list[v1].push_back(ii(v2, w));
            list[v2].push_back(ii(v1, w));
        }
        ms = dijkstra(s, t, n);

        cout << "Case #" << ++c << ": ";
        if (ms < 0)
            cout << "unreachable" << endl;
        else
            cout << ms << endl;

        list.clear();
    }
    return 0;
}
