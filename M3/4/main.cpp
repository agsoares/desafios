//10008 - What's Cryptanalysis
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;
typedef pair<int, int> ii;
typedef pair<char, ii> cii;

struct sort_pred {
    bool operator()(const cii &left, const cii &right) {
        if (left.second.first == right.second.first) {
            return left.first < right.first;
        }
        return left.second.first > right.second.first;
    }
};

int main(int argc, char** argv) {
    int n = 0, counter = 0;
    cin >> n;
    map<char, ii> character;
    string line;
    getline(cin, line);

    for (int k = 0; k < n; k++) {
        getline(cin, line);
        for (int i = 0; i < line.size(); i++) {
            char c = line[i];
            if (isalpha(c)) {
                c = toupper(c);
                if (character.count(c) > 0) {
                    character[c].first++;
                } else {
                    character[c] = ii(1, counter+i);
                }
            }
        }

        counter += line.size();

    }
    vector<cii> order;
    for (map<char, ii>::iterator it = character.begin(); it != character.end(); it++) {
        order.push_back(make_pair(it->first, it->second));
    }

    sort(order.begin(), order.end(), sort_pred());

    for (vector<cii>::iterator it = order.begin(); it != order.end(); it++) {
        cout << it->first << " " << it->second.first << endl;
    }


    return 0;
}
