//10130 - SuperSale
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cctype>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef pair<int, int> ii;
typedef vector<ii> vii;


struct value
{
  inline bool operator() (ii p1, ii p2) {
    return p1.first/p1.second > p2.first/p2.second;
  }
};

int main(int argc, char** argv) {
    int t, n, p, w, g, mw;
    cin >> t;
    for (int k = 0; k < t; k++) {
      cin >> n;
      vii items;
      for (int i = 0; i < n; i++) {
        cin >> p >> w;
        items.push_back(ii(p, w));
      }
      sort(items.begin(), items.end(), value());
      /*
      for (vii::iterator j = items.begin(); j != items.end(); j++) {
        cout << j->first << endl;
      }
      */
      cin >> g;
      int group[g];
      for (int i = 0; i < g; i++) {
        cin >> mw;
        group[i] = mw;
      }
      sort(group, group+g);
      for (int i = 0; i< g; i++) {
        for (vii::iterator j = items.begin(); j != items.end(); j++) {
          cout << j->first << endl;
        }
      }

    }

    return 0;
}
