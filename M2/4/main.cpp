//10305 - Ordering Tasks
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cctype>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef vector<int> vi;
vector <vi> list;
int counter[100] = { 0 };

int main(int argc, char** argv) {
    int n, m, u, v;
    while (cin >> n >> m) {
      if (n == 0 && m == 0) break;
      int t = 0;
      for (int i = 0; i < n; i++) {
        counter[i] = 0;
      }
      list.assign(n, vi());
      for (int i = 0; i < m; i++) {
        cin >> u >> v;
        u--; v--;
        list[u].push_back(v);
        counter[v]++;
      }
      queue <int> tasks;
      while (t <  n) {
        for (int i = 0; i < n; i++) {
          if (counter[i] == 0) {
            counter[i]--;
            tasks.push(i);
          }
        }

        while (!tasks.empty()) {
          int sel = tasks.front(); tasks.pop();
          if (t > 0) cout << " ";
          cout << sel+1;
          for (vi::iterator j = list[sel].begin(); j != list[sel].end(); j++) {
            counter[*j]--;
          }
          t++;
        }

      }
      list.clear();
      cout << endl;
    }

    return 0;
}
