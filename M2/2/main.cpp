//12289 - One-Two-Three
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cctype>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;


int isEqual(string str, string comparer) {
  if (str.compare(comparer) == 0) return 1;
  for (int i = 0; i <  comparer.length(); i++) {
    int equal = 1;
    for (int j = 0; j < comparer.length(); j++) {
        if (i == j) continue;
        if (str[j] != comparer[j]) equal = 0;
    }
    if (equal) return 1;
  }
  return 0;
}

int main(int argc, char** argv) {
    int n;
    cin >> n;
    string str;
    getline(cin, str);
    for (int i = 0; i < n; i++) {
      getline(cin, str);
      if (isEqual(str, "one") == 1) cout << "1";
      else if (isEqual(str, "two") == 1) cout << "2";
      else cout << "3";
      cout << endl;
    }

    return 0;
}
