//11995 - I Can Guess the Data Structure!
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>

#include <queue>
#include <stack>
#include <string>
#include <vector>
#include <functional>

using namespace std;
int main(int argc, char** argv) {
    int n;
    while (cin >> n) {
        priority_queue<int> pq;
        queue<int> q;
        stack<int> s;
        int isPQ = 1, isQ = 1, isS = 1;
        for (int i = 0; i < n; i++ ) {
            int op, num;
            cin >> op >> num;
            if (op == 1) {
                pq.push(num); q.push(num); s.push(num);
            } else {
                if (!pq.empty()) {
                    if (isPQ == 1 && pq.top()  != num) isPQ=0;
                    if (isQ == 1  && q.front() != num) isQ=0;
                    if (isS == 1  && s.top()   != num) isS=0;
                    pq.pop(); q.pop(); s.pop();
                } else {
                    isPQ=0; isQ=0; isS=0;
                }
            }
        }
        string ans;
        if (isPQ + isQ + isS > 1) {
            ans = "not sure";
        } else if (isPQ + isQ + isS == 0) {
            ans = "impossible";
        } else {
            if (isPQ) {
                ans = "priority queue";
            } else if (isQ) {
                ans = "queue";
            } else {
                ans = "stack";
            }
        }
        cout << ans << endl;
    }
    return 0;
}
