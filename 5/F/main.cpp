//357 - Let Me Count Ways
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;
unsigned long int ways[30005];
int c[] = {1, 5, 10, 25, 50};

int main(int argc, char** argv) {
    int n;
    ways[0] = 1;
    for (int i = 0; i < 5; i++) {
        int coin = c[i];
        for(int j = coin; j < 30005; j++) {
            ways[j] += ways[j-coin];
        }
    }
    while (cin >> n) {
        if (ways[n] > 1) {
            cout << "There are " << ways[n] <<  " ways to produce " << n << " cents change." << endl;
        } else {
            cout << "There is only 1 way to produce " << n << " cents change." << endl;
        }
    }
    return 0;
}
