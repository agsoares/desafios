//11286 - Conformity
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int n;
    while (cin >> n) {
        if (n == 0) break;
        map<string, int> counter;

        for (int i = 0; i < n; i++) {
            string id;
            string course[5];
            for (int j = 0; j < 5; j++) {
                cin >> course[j];
            }
            sort(course, course+5);
            for (int j = 0; j < 5; j++) {
                id.append(course[j]);
            }
            counter[id]++;

        }
        int max = 0; int ans = 0;
        for (map<string, int>::iterator it = counter.begin(); it != counter.end(); it++) {
            if (it->second >  max) {
                ans = 0;
                max = it->second;
            }
            if (it->second == max) ans += max;
        }
        cout << ans << endl;
    }
    return 0;
}
