//10405 - Longest Common Subsequence
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    string str1, str2;
    while (getline(cin, str1) && getline(cin, str2)) {
        int map[str1.length()+1][str2.length()+1];
        for (int i = 0; i <= str1.length(); i++) {
            for (int j = 0; j <= str2.length(); j++) {
                if (i == 0 || j == 0) {
                    map[i][j] = 0;
                } else {
                    if (str1[i-1] == str2[j-1]) {
                        map[i][j] = map[i-1][j-1] +1;
                    } else {
                        map[i][j] = map[i][j-1] >= map[i-1][j] ? map[i][j-1] : map[i-1][j];
                    }
                }
            }
        }
        cout << map[str1.length()][str2.length()] << endl;
    }
    return 0;
}
