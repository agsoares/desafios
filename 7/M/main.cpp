//10943 - How do you add?
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

#define MAX_N  101
#define MAX_K  101

int val[MAX_N][MAX_K];
void calc() {
    for (int i = 0; i < MAX_N; i++) {
        for (int j = 0; j < MAX_K; j++) {
            if(j == 0) val[i][j] = 0;
            else if (i == 0 || j == 1) val[i][j] = 1;
            else {
                val[i][j] = (val[i-1][j] + val[i][j-1])%1000000;
            }
        }

    }
}

int main(int argc, char** argv) {
    int n, k;
    calc();
    while (cin >> n && cin >> k) {
        if (n == 0 && k == 0) break;
        cout << val[n][k] << endl;

    }
    return 0;
}
