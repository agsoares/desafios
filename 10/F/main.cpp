//11827 - Maximum GCD
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <sstream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int GCD(int u, int v) {
    while ( v != 0) {
        int r = u % v;
        u = v;
        v = r;
    }
    return u;
}

int main(int argc, char** argv) {
    int T;
    cin >> T;
    string line;
    getline(cin, line);
    while (T--) {
        getline(cin, line);
        istringstream is(line);
        vector<int> vals;
        int best = 0;
        for (int x; is >> x; ) {
            vals.push_back(x);
        }
        sort(vals.begin(), vals.end(), less<int>());
        for (int i = 0; i < vals.size(); i++) {
            for (int j = i+1; j < vals.size(); j++) {
                if (best > vals[i] || best > vals[j]) continue;
                int gcd = GCD(vals[i], vals[j]);
                if (gcd > best) best = gcd;
            }
        }
        cout << best << endl;
    }
    return 0;
}
