//10954 - Add All
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int n;
    while (cin >> n) {
        if (n == 0) break;
        queue<int> discard;
        queue<int> deck;
        for (int i = 1; i <= n; i++) {
            deck.push(i);
        }
        cout << "Discarded cards:";
        while (deck.size() > 1) {
            cout << " " << deck.front();
            discard.push(deck.front());
            deck.pop();
            deck.push(deck.front());
            deck.pop();
            if (deck.size() > 1) cout << ",";
        }
        cout << endl;
        cout << "Remaining card: " << deck.front() << endl;
    }
    return 0;
}
