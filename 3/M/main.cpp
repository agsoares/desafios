//
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;
bool isInvalid(char x) { return !(isalpha(x) || x == '-'); }

int main(int argc, char** argv) {
    vector<string> words;
    string word;
    while (cin >> word) {
        transform(word.begin(), word.end(), word.begin(), ::tolower);
        size_t pos = 0;
        string token;
        replace_if(word.begin(), word.end(), isInvalid, ' ');
        while((pos = word.find(' ')) != string::npos) {
            token = s.substr(0, pos);
            std::cout << token << std::endl;
            s.erase(0, pos + delimiter.length());
            while (token[token.size()-1] == '-') {
                string rest;
                cin >> rest;
                token = token.substr(0,token.size()-1);
                token.append(rest);
            }
            if (words.empty() || find(words.begin(), words.end(), word) == words.end()) {
                words.push_back(token);
            }

        }

    }
    sort(words.begin(), words.end());
    for(vector<string>::iterator it = words.begin(); it != words.end(); ++it) {
        cout << *it << endl;
    }
    return 0;
}
