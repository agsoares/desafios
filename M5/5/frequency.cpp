#include <iostream>

using namespace std;

int main(int argc, char** argv){
  string input;
  unsigned counters[26];
  unsigned tc, max;
  cin >> tc;
  cin.ignore();
  for(unsigned c=0; c<tc; ++c){
      getline(cin,input);
      for(unsigned i=0; i<26; ++i)counters[i]=0;
      max=0;
      for(string::iterator it=input.begin(); it!=input.end();++it){
          if(*it>='A'&&*it<='Z'){
              *it=*it-'A'+'a';
          }
          if(*it>='a'&&*it<='z'){
              ++counters[*it-'a'];
              if(counters[*it-'a']>max)max=counters[*it-'a'];
          }
      }
         for(unsigned i=0; i<26; ++i) if(counters[i]==max)cout<<(char)(i+'a');
         cout << endl;
  }

  return 0;
}
