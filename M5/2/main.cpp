//10664 - Luggage
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <sstream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;


int main(int argc, char** argv) {
    int T;
    cin >> T;
    string line;
    getline(cin, line);
    while (T--) {
        getline(cin, line);
        istringstream is(line);
        vector<int> luggages;
        int size = 0;
        for (int x; is >> x;) {
            size += x;
            luggages.push_back(x);
        }
        if (size%2 == 1) {
            cout << "NO" << endl;
            continue;
        }
        int n = luggages.size();
        // cout << "n : " << n << endl;
        // cout << "size :" << size << endl;
        int mat[luggages.size()+1][(size/2)+1];
        for (int i = 0; i <= luggages.size(); ++i)
        {
            for (int j = 0; j <= size/2; ++j)
            {
                mat[i][j] = 0;
            }
        }
        // cout << "luggages : " ;
        // for (int i = 0; i < n; ++i)
        // {
            // cout << luggages[i] << " ";
        // }
        // cout << endl;
        for (int i = 1; i <= luggages.size(); i++ ) {
            for (int j = 0; j <= size/2; j++ ) {
                mat[i][j] = mat[i-1][j];
                if (j >= luggages[i-1]) {
                    mat[i][j] = max(mat[i-1][j-luggages[i-1]]+luggages[i-1], mat[i-1][j]);
                }
                // cout << mat[i][j] << " ";
            }
            // cout << endl;
        }
        // cout << mat[luggages.size()][size/2] << endl;


        if (mat[luggages.size()][size/2] == size/2) {
            cout << "YES" << endl;

        } else {
            cout << "NO" << endl;
        }

    }

    return 0;
}
