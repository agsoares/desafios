#include <iostream>

using namespace std;


#include <list>
#include <iostream>

using namespace std;

int main (int argc, char** argv){
  list<unsigned> primes;
  unsigned tc, max, rest, n;
  primes.push_back(2);
  for(unsigned i=2; i<5000; ++i){
     primes.push_back(i*2-1);
  }

  for(list<unsigned>::iterator it=primes.begin(); it!=primes.end()&&*it<1000; ++it){
     list<unsigned>::iterator jt(it);
     while(++jt!=primes.end()){
        if(*jt % *it==0){
           list<unsigned>::iterator kt=jt;
           jt--;
           primes.erase(kt);
        }
     }
  }

  for(cin>>tc; tc>0; --tc){
      cin>>n;
      rest = 0;
      max=0;
      for (list<unsigned>::iterator it=primes.begin(); it!=primes.end() && *it<=n; ++it){
          if(n%(*it)>rest){
              max=*it;
              rest=n%*it;
          }
      }
      cout << max << endl;
  }
  return 0;
}
