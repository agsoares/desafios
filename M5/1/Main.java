import java.util.Scanner;
import java.util.Queue;
import java.util.LinkedList;


class Main {

	public static void main(String[] args) {

		Scanner scin = new Scanner(System.in);
		int n; int r;
		int nbScenar = 0;

		while (true) {
			nbScenar++;
			n = scin.nextInt();
			r = scin.nextInt();
			if (n==0 && r ==0) {
				break;
			}
			int[][] maxPassenger = new int[n+1][n+1];

			for (int i=0; i<r; i++) {
				int c1 = scin.nextInt();
				int c2 = scin.nextInt();
				int p = scin.nextInt();

				maxPassenger[c1][c2] = p;
				maxPassenger[c2][c1] = p;
			}
			int start = scin.nextInt();
			int end = scin.nextInt();
			int nbPass = scin.nextInt();

			int nbTrip = 0;
			while (true) {
				nbTrip++;
				boolean[] visited = new boolean[n+1];
				Queue<Integer> stack = new LinkedList<Integer>();
				stack.add(start);
				visited[start] = true;
				boolean found = false;

				while(!stack.isEmpty()) {
					int x = stack.poll();
					if (x == end) {
						break;
					}
					for (int i=1; i<=n; i++) {
						if (!visited[i] && (maxPassenger[x][i]-1)*nbTrip >= nbPass) {
							visited[i] = true;
							stack.add(i);
						}
					}
				}

				if (visited[end]) {
					break;
				}

			}
			System.out.println("Scenario #" + nbScenar);
			System.out.println("Minimum Number of Trips = " + nbTrip);
			System.out.println();
		}
	}


}