//11364 - Parking
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;


int main(int argc, char** argv) {
    int T;
    cin >> T;
    while (T--) {
        int n, s;
        cin >> n;
        vector<int> stores;
        for (int i = 0; i < n; i++) {
            cin >> s;
            stores.push_back(s);
        }
        sort(stores.begin(), stores.end());
        int best = -1;
        for (int i = stores[0]; i <= stores[n-1]; i++) {
            int dist = 0;
            dist += i-stores[0];
            dist += stores[n-1]-stores[0];
            dist += stores[n-1]-i;
            if (best == -1 || best >= dist) {
                best = dist;
            }
        }
        cout << best << endl;
    }

    return 0;
}
