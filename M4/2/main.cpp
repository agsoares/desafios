//12150 - Pole Position
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int n;
    while (cin >> n) {
        if (n == 0) break;
        int positions[n];
        bool impossible = false;
        for (int i = 0; i < n; i++) {
            positions[i] = -1;
        }
        for (int i = 0; i < n; i++) {
            int car, pos;
            cin >> car >> pos;
            if (pos+i > n || pos+i < 0) {
                impossible = true;
                continue;
            }
            if (positions[pos+i] != -1) {
                impossible = true;
                continue;
            }
            positions[pos+i] = car;
        }
        if (impossible) {
            cout << -1 << endl;
            continue;
        }
        for (int i = 0; i < n; i++) {
            if (i > 0) cout << " ";
            cout << positions[i];
        }
        cout << endl;
    }
    return 0;
}
