//12150 - Pole Position
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

typedef pair<int, float> ii;
typedef vector<ii> vii;

ii* coords;
vector <vii> list;

bool *visited;

float roads;
float railroads;
int states;

int prim(int vi, int n, int a) {
    priority_queue< pair<float,int>, vector< pair<float,int> >, greater< pair<float,int> > > q;
    pair<float,int> p;
    int v;
    float w, ret = 0;
    q.push( make_pair(0, vi));
    while (!q.empty()) {
        p = q.top(); q.pop();
        v = p.second;
        w = p.first;
        if (visited[v] == true) continue;
        visited[v] = true;
        if (w >= a) {
            railroads += w;
            states++;
        } else {
            roads += w;
        }
        ret += w;
        for (int i = 0; i < list[v].size(); ++i)
            if (!visited[list[v][i].first]) {
                q.push( make_pair( list[v][i].second, list[v][i].first ) );
            }
    }
    return ret;
}

int all_visited(int n) {
    for (int i = 0; i < n; i++) {
        if (visited[i] == false) return i;
    }
    return -1;
}

int main(int argc, char** argv) {
    int T, t = 0;
    cin >> T;
    while (++t <= T) {
        roads = 0;
        railroads= 0;
        states = 1;
        int cities, threshold;
        cin >> cities >> threshold;
        coords = new ii[cities];
        list.assign(cities, vii());
        visited = new bool[cities];
        for (int i = 0; i < cities; i++) {
            int x, y;
            cin >> x >> y;
            for (int j = 0; j < i; j++) {
                float dist = hypot(coords[j].first-x, coords[j].second-y);
                list[i].push_back(ii(j, dist));
                list[j].push_back(ii(i, dist));
            }
            coords[i] = ii(x, y);
            visited[i] = false;
        }
        prim(0, cities, threshold);

        cout << "Case #" << t << ": ";
        cout << states << " ";
        cout << round(roads) << " ";
        cout << round(railroads) << endl;
    }

    return 0;
}
