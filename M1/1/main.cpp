#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    string load;
    int n = 0;
    while (cin >> load) {
        if (load.compare("end") == 0) break;
        char c[26];
        int stacks = 0;
        n++;
        for (int i = 0; i < load.size(); i++) {
            int insert_stack = -1;
            int stack_difference = 1000;
            for (int j = 0; j < stacks; j++) {
                int difference = c[j]-load[i];
                if (difference >= 0) {
                    if (difference < stack_difference ) {
                        stack_difference = difference;
                        insert_stack = j;
                    }
                }
            }
            if (insert_stack < 0) {
                c[stacks] = load[i];
                stacks++;
            } else {
                c[insert_stack] = load[i];
            }
        }


        cout << "Case " << n << ": ";
        cout << stacks << endl;
    }
    return 0;
}
