#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int notes = 0;

    while (cin >> notes) {
        if (notes == 0) break;
        int magnitudes[notes];
        int peaks = 0;
        for (int i = 0; i < notes; i++) {
            cin >> magnitudes[i];
        }
        for (int i = 0; i < notes; i++) {
            int prev = i-1 < 0 ? notes-1 : i-1;
            int next = ((i+1)%notes);
            if (magnitudes[prev] < magnitudes[i] && magnitudes[next] < magnitudes[i])
                peaks++;
            if (magnitudes[prev] > magnitudes[i] && magnitudes[next] > magnitudes[i])
                peaks++;
        }
        cout << peaks << endl;

    }
    return 0;
}
