#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cctype>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int tests = 0;
    cin >> tests;
    string stripline;
    getline(cin, stripline);
    for (int i = 0; i < tests; i++) {
        string in, out;
        getline(cin, in);
        getline(cin, out);
        cout << "Case ";
        cout << i+1 << ": ";
        if (in.compare(out) == 0) {
            cout << "Yes";
        } else {
            in.erase(remove_if(in.begin(), in.end(), (int(*)(int))isspace), in.end());

            if (in.compare(out) == 0) {
                cout << "Output Format Error";
            } else {
                cout << "Wrong Answer";
            }
        }

        cout << endl;
    }
    return 0;
}
