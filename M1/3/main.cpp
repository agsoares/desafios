#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int tests = 0;
    while (cin >> tests) {
        if (tests == 0) break;
        int N, M;
        cin >> N >> M;
        for (int i = 0; i < tests; i++) {
            int X, Y;
            cin >> X >> Y;
            if (N == X || M == Y ) {
                cout << "divisa" << endl;
                continue;
            }
            if (Y > M) {
                cout << "N";
            } else {
                cout << "S";
            }
            if (X > N) {
                cout << "E";
            } else {
                cout << "O";
            }
            cout << endl;
        }
    }
    return 0;
}
