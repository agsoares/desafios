//941 - Permutations
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    int tests;
    long long n;
    string str;
    cin >> tests;
    for (int t = 0; t < tests; t++) {
        while (cin >> str && cin >> n) {
            sort(str.begin(), str.end());
            cout << str << endl;
        }
    }
    return 0;
}
