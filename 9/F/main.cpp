//10252 - Common Permutation
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

int main(int argc, char** argv) {
    string str1, str2;
    while (getline(cin, str1) && getline(cin, str2)) {
        vector<char> x;
        for (int i = 0; i <= str1.length(); i++) {
            for (int j = 0; j <= str2.length(); j++) {
                if (!islower(str1[i])) break;
                if (!islower(str2[j])) continue;
                if (str1[i] == str2[j]) {
                    x.push_back(str1[i]);
                    str1[i] = toupper(str1[i]);
                    str2[j] = toupper(str2[j]);
                }
            }
        }
        sort(x.begin(), x.end());
        for(vector<char>::iterator it = x.begin(); it != x.end(); ++it) {
            cout << *it;
        }
        cout << endl;
        x.clear();
    }
    return 0;
}
