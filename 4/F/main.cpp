//725 - Division
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

#include <sstream>
#include <iomanip>

using namespace std;



int main(int argc, char** argv) {
    int n, first = 0;
    while (cin >> n) {
        if (n == 0) break;
        if (first != 0) cout << endl;
        first = 1;
        if (n > 79) {
            cout << "There are no solutions for " << n << "." << endl << endl;
            continue;
        }
        int found = 0;
        for (int i = 1234; i*n < 98765; i++) {
            int testing[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            int invalid = 0;
            stringstream buffer;
            buffer.str("");
            buffer << setw(5) << setfill('0') << i << endl;
            for (int j=0; j < 5; j++) {
                int pos = buffer.str()[j]-'0';
                if (testing[pos] != 0) {
                    invalid = 1;
                    break;
                }
                testing[pos] = 1;
            }
            if (invalid == 1) continue;
            buffer.str("");
            buffer << setw(5) << setfill('0') << i*n << endl;
            for (int j=0; j < 5; j++) {
                int pos = buffer.str()[j]-'0';
                if (testing[pos] != 0) {
                    invalid = 1;
                    break;
                }
                testing[pos] = 1;
            }
            if (invalid == 1) continue;
            cout << setw(5) << setfill('0') << i*n << " / ";
            cout << setw(5) << setfill('0') << i   << " = ";
            cout << n << endl;
            found++;

        }
        if (found == 0) cout << "There are no solutions for " << n << "." << endl;
    }
    return 0;
}
