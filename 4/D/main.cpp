//1237 - Expert Enough?
#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

#include <iomanip>

using namespace std;

typedef struct data {
    string M;
    int L;
    int H;
} data;

data database[10000];

int main(int argc, char** argv) {
    int t;
    cin >> t;
    for (int tests=0; tests<t; tests++) {
        int p, q;
        cin >> p;
        for(int i = 0; i<p; i++) {
            cin >> database[i].M >> database[i].L >> database[i].H;
        }
        cin >> q;
        for(int i = 0; i<q; i++) {
            int price, found = -1;
            cin >> price;
            for (int j = 0; j<p; j++) {
                if (price >= database[j].L && price <= database[j].H ) {
                    if (found >= 0) {
                        found = -1;
                        break;
                    }
                    found = j;
                }

            }
            if (found < 0) cout << "UNDETERMINED" << endl;
            else cout << database[found].M << endl;

        }
        if (tests<t-1) cout << endl;
    }


    return 0;
}
